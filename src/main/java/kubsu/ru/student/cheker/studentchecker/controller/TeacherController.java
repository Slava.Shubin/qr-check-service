package kubsu.ru.student.cheker.studentchecker.controller;

import kubsu.ru.student.cheker.studentchecker.controller.dto.request.LoginRequest;
import kubsu.ru.student.cheker.studentchecker.controller.dto.response.LoginTeacherResponse;
import kubsu.ru.student.cheker.studentchecker.model.Teacher;
import kubsu.ru.student.cheker.studentchecker.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@CrossOrigin("*")
@RequestMapping(path = "/kubsu/teacher", produces = "application/json")
public class TeacherController {
    private final TeacherService teacherService;

    @GetMapping("{teacherId}")
    public Teacher getTeacherById(@PathVariable Long teacherId) {
        return teacherService.getTeacherById(teacherId);
    }

    @PostMapping("/login")
    public LoginTeacherResponse loginTeacher(@RequestBody LoginRequest request) {
        return teacherService.loginTeacher(request.getLogin(), request.getPassword());
    }

    @PostMapping
    public void addTeacher(@RequestBody Teacher student) {
        teacherService.addTeacher(student);
    }
}
