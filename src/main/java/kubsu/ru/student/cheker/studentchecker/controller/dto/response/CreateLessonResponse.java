package kubsu.ru.student.cheker.studentchecker.controller.dto.response;

import kubsu.ru.student.cheker.studentchecker.controller.dto.request.CreateLessonRequest;
import kubsu.ru.student.cheker.studentchecker.model.Lesson;
import kubsu.ru.student.cheker.studentchecker.model.Teacher;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateLessonResponse {
    private Long lessonId;
    private Long teacherId;

    public static CreateLessonResponse fromLessonAndTeacher(Lesson lesson) {
        return CreateLessonResponse.builder()
                .lessonId(lesson.getId())
                .teacherId(lesson.getTeacher().getId())
                .build();
    }
}
