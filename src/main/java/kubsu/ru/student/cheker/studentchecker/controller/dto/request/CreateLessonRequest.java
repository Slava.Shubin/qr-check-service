package kubsu.ru.student.cheker.studentchecker.controller.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateLessonRequest {
    private Long teacherId;
    private String lessonName;
    private String lessonTheme;
    private Date startLessonDate;
}
