package kubsu.ru.student.cheker.studentchecker.repository;

import kubsu.ru.student.cheker.studentchecker.model.Student;
import kubsu.ru.student.cheker.studentchecker.model.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Long> {
    Teacher findTeacherByLogin(String login);
}
