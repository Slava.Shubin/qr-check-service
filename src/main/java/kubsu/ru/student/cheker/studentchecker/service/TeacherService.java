package kubsu.ru.student.cheker.studentchecker.service;

import kubsu.ru.student.cheker.studentchecker.controller.dto.response.LoginTeacherResponse;
import kubsu.ru.student.cheker.studentchecker.exeption.EntityNotFoundException;
import kubsu.ru.student.cheker.studentchecker.exeption.FailedLoginUserException;
import kubsu.ru.student.cheker.studentchecker.model.Teacher;
import kubsu.ru.student.cheker.studentchecker.model.Teacher;
import kubsu.ru.student.cheker.studentchecker.repository.TeacherRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TeacherService {
    private final TeacherRepository teacherRepository;
    public Teacher getTeacherById(Long teacherId) {
        Teacher teacher = teacherRepository.findById(teacherId)
                .orElseThrow(EntityNotFoundException.teacherNotFound(teacherId));
        teacher.setPassword(null);
        return teacher;
    }

    public Teacher getTeacherByLogin(String teacherLogin) {
        return teacherRepository.findTeacherByLogin(teacherLogin);
    }

    public LoginTeacherResponse loginTeacher(String login, String password) {
        Teacher teacher = teacherRepository.findTeacherByLogin(login);
        if (!teacher.getPassword().equals(password)) {
            throw FailedLoginUserException.teacherFailedRegister();
        }
        return LoginTeacherResponse.fromTeacher(teacher);
    }

    public void addTeacher(Teacher teacher) {
        teacher.hashPassword();
        teacherRepository.save(teacher);
    }
}
