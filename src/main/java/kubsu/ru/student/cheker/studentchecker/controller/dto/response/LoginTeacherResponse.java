package kubsu.ru.student.cheker.studentchecker.controller.dto.response;

import kubsu.ru.student.cheker.studentchecker.model.Teacher;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoginTeacherResponse {
    private Long teacherId;
    private String message;

    public static LoginTeacherResponse fromTeacher(Teacher teacher) {
        return LoginTeacherResponse.builder()
                .teacherId(teacher.getId())
                .message("Register Success")
                .build();
    }
}
