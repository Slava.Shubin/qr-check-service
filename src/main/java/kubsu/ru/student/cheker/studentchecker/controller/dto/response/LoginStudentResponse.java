package kubsu.ru.student.cheker.studentchecker.controller.dto.response;

import kubsu.ru.student.cheker.studentchecker.model.Student;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoginStudentResponse {
    private Long studentId;
    private String message;

    public static LoginStudentResponse fromStudent(Student student) {
        return LoginStudentResponse.builder()
                .studentId(student.getId())
                .message("Register Success")
                .build();
    }
}
