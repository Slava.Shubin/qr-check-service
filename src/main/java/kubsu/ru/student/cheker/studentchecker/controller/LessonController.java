package kubsu.ru.student.cheker.studentchecker.controller;

import kubsu.ru.student.cheker.studentchecker.controller.dto.request.CreateLessonRequest;
import kubsu.ru.student.cheker.studentchecker.controller.dto.response.CreateLessonResponse;
import kubsu.ru.student.cheker.studentchecker.controller.dto.response.MarkStudentResponse;
import kubsu.ru.student.cheker.studentchecker.service.LessonService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@CrossOrigin("*")
@RequestMapping(path = "/kubsu/lesson", produces = "application/json")
public class LessonController {
    private final LessonService lessonService;

    @PostMapping("/markStudent")
    public MarkStudentResponse markStudent(@RequestParam String studentLogin, @RequestParam Long lessonId) {
        return lessonService.markStudent(studentLogin, lessonId);
    }

    @PostMapping
    public CreateLessonResponse createLesson(@RequestBody CreateLessonRequest request) {
        return lessonService.createLesson(request);
    }
}
