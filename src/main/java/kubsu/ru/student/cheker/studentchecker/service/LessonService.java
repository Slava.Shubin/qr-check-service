package kubsu.ru.student.cheker.studentchecker.service;

import kubsu.ru.student.cheker.studentchecker.controller.dto.request.CreateLessonRequest;
import kubsu.ru.student.cheker.studentchecker.controller.dto.response.CreateLessonResponse;
import kubsu.ru.student.cheker.studentchecker.controller.dto.response.MarkStudentResponse;
import kubsu.ru.student.cheker.studentchecker.exeption.EntityNotFoundException;
import kubsu.ru.student.cheker.studentchecker.model.Lesson;
import kubsu.ru.student.cheker.studentchecker.model.Student;
import kubsu.ru.student.cheker.studentchecker.model.Teacher;
import kubsu.ru.student.cheker.studentchecker.repository.LessonRepository;
import kubsu.ru.student.cheker.studentchecker.repository.TeacherRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LessonService {
    private final LessonRepository lessonRepository;
    private final TeacherRepository teacherRepository;
    private final StudentService studentService;

    public MarkStudentResponse markStudent(String studentLogin, Long lessonId) {
        Student studentByLogin = studentService.getStudentByLogin(studentLogin);
        if (studentByLogin != null) {
            Lesson lesson = lessonRepository.findById(lessonId)
                    .orElseThrow(EntityNotFoundException.lessonNotFound(lessonId));
            lesson.getStudents().add(studentByLogin);
            return MarkStudentResponse.fromLessonAndStudent(studentByLogin, lesson, true);
        }
        throw EntityNotFoundException.studentNotFound(studentLogin);
    }

    public CreateLessonResponse createLesson(CreateLessonRequest request) {
        Teacher teacher = teacherRepository.findById(request.getTeacherId())
                .orElseThrow(EntityNotFoundException.studentNotFound(request.getTeacherId()));
        Lesson lesson = Lesson.createLessonFromRequest(request, teacher);
        lessonRepository.save(lesson);
        if (lesson.getId() == null) {
            throw new RuntimeException("Failed to create lesson");
        }
        return CreateLessonResponse.fromLessonAndTeacher(lesson);
    }
}
