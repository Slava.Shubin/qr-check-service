package kubsu.ru.student.cheker.studentchecker.controller;

import kubsu.ru.student.cheker.studentchecker.controller.dto.request.LoginRequest;
import kubsu.ru.student.cheker.studentchecker.controller.dto.response.LoginStudentResponse;
import kubsu.ru.student.cheker.studentchecker.model.Student;
import kubsu.ru.student.cheker.studentchecker.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@CrossOrigin("*")
@RequestMapping(path = "/kubsu/student", produces = "application/json")
public class StudentController {
    private final StudentService studentService;

    @GetMapping("{studentId}")
    public Student getStudentById(@PathVariable Long studentId) {
        return studentService.getStudentById(studentId);
    }

    @PostMapping("/login")
    public LoginStudentResponse loginStudent(@RequestBody LoginRequest request) {
        return studentService.loginStudent(request.getLogin(), request.getPassword());
    }

    @PostMapping
    public void addStudent(@RequestBody Student student) {
        studentService.addStudent(student);
    }
}
