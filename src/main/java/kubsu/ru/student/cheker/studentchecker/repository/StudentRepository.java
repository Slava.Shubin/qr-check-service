package kubsu.ru.student.cheker.studentchecker.repository;

import kubsu.ru.student.cheker.studentchecker.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    Student findStudentByLogin(String login);
}
