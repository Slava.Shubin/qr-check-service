package kubsu.ru.student.cheker.studentchecker.model;

import jakarta.persistence.*;
import kubsu.ru.student.cheker.studentchecker.controller.dto.request.CreateLessonRequest;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;


@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "lessons")
public class Lesson {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "lesson_name", nullable = false)
    private String lessonName;

    @Column(name = "lesson_theme", nullable = false)
    private String lessonTheme;

    @OneToOne
    @JoinColumn(name = "teacher_id", referencedColumnName = "id")
    private Teacher teacher;

    /**
     * Время начала занятия
     */
    @Column(name = "start_lesson_date", nullable = false)
    private Date startLessonDate;

    @Column(name = "end_lesson_date", nullable = false)
    private Date endLessonDate;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "lesson_students",
            joinColumns = @JoinColumn(name = "lesson_id"),
            inverseJoinColumns = @JoinColumn(name = "student_id"))
    private Set<Student> students = new HashSet<>();

    public static Lesson createLessonFromRequest(CreateLessonRequest request, Teacher teacher) {
        return Lesson.builder()
                .lessonName(request.getLessonName())
                .lessonTheme(request.getLessonTheme())
                .teacher(teacher)
                .startLessonDate(request.getStartLessonDate())
                .endLessonDate(new Date(request.getStartLessonDate().getTime() + 4800000L))
                .build();
    }
}
