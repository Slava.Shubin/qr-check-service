package kubsu.ru.student.cheker.studentchecker.controller.dto.response;

import kubsu.ru.student.cheker.studentchecker.model.Lesson;
import kubsu.ru.student.cheker.studentchecker.model.Student;
import kubsu.ru.student.cheker.studentchecker.model.view.StudentView;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MarkStudentResponse {
    private Long studentId;
    private Boolean markSuccess;
    private Long lessonId;
    /**
     * Список присутствующих студентов
     */
    private List<StudentView> awardStudents;

    public static MarkStudentResponse fromLessonAndStudent(Student student, Lesson lesson, Boolean isMark) {
        return MarkStudentResponse.builder()
                .studentId(student.getId())
                .markSuccess(isMark)
                .lessonId(lesson.getId())
                .awardStudents(new ArrayList<>(lesson.getStudents()).stream().map(StudentView::fromStudent).toList())
                .build();
    }
}
