package kubsu.ru.student.cheker.studentchecker.repository;

import kubsu.ru.student.cheker.studentchecker.model.Lesson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LessonRepository extends JpaRepository<Lesson, Long> {
}
