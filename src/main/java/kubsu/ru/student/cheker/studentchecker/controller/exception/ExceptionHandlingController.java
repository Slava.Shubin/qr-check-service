package kubsu.ru.student.cheker.studentchecker.controller.exception;

import kubsu.ru.student.cheker.studentchecker.controller.dto.response.ResponseMessage;
import kubsu.ru.student.cheker.studentchecker.exeption.EntityNotFoundException;
import kubsu.ru.student.cheker.studentchecker.exeption.FailedLoginUserException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandlingController {

    @ExceptionHandler({FailedLoginUserException.class})
    public ResponseEntity<ResponseMessage> handleFailedRegisterUserException(FailedLoginUserException e) {
        return new ResponseEntity<>(new ResponseMessage(e.getMessage()), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler({RuntimeException.class, EntityNotFoundException.class})
    public ResponseEntity<ResponseMessage> handleRuntimeException(Exception e) {
        return new ResponseEntity<>(new ResponseMessage(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
