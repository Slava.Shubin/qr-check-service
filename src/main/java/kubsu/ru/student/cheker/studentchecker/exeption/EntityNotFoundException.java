package kubsu.ru.student.cheker.studentchecker.exeption;

import java.util.function.Supplier;

public class EntityNotFoundException extends RuntimeException implements Supplier<EntityNotFoundException> {

    private EntityNotFoundException(String message) {
        super(message);
    }

    public static EntityNotFoundException studentNotFound(Object studentId) {
        return new EntityNotFoundException("Student with id = " + studentId + "not found");
    }

    public static EntityNotFoundException teacherNotFound(Object teacherId) {
        return new EntityNotFoundException("Teacher with id = " + teacherId + "not found");
    }

    public static EntityNotFoundException lessonNotFound(Long lessonId) {
        return new EntityNotFoundException("Lesson with id = " + lessonId + "not found");
    }

    /**
     * Gets a result.
     *
     * @return a result
     */
    @Override
    public EntityNotFoundException get() {
        return this;
    }
}
