package kubsu.ru.student.cheker.studentchecker.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import kubsu.ru.student.cheker.studentchecker.service.utils.HashingUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Entity(name = "teachers")
public class Teacher {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "second_name", nullable = false)
    private String secondName;

    @Column(name = "login", nullable = false, unique = true)
    private String login;
    @Setter
    @Column(name = "password", nullable = false)
    private String password;

    public void hashPassword() {
        password = HashingUtils.hashPassword(password);
    }
}
