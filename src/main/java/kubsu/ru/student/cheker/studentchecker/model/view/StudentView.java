package kubsu.ru.student.cheker.studentchecker.model.view;

import kubsu.ru.student.cheker.studentchecker.model.Student;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentView {
    private String name;
    private String secondName;
    private String group;

    public static StudentView fromStudent(Student student) {
        return StudentView.builder()
                .name(student.getName())
                .secondName(student.getSecondName())
                .group(student.getGroup())
                .build();
    }
}
