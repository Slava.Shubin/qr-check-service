package kubsu.ru.student.cheker.studentchecker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentCheckerApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudentCheckerApplication.class, args);
    }

}
