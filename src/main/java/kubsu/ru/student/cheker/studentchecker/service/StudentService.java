package kubsu.ru.student.cheker.studentchecker.service;

import kubsu.ru.student.cheker.studentchecker.controller.dto.response.LoginStudentResponse;
import kubsu.ru.student.cheker.studentchecker.exeption.EntityNotFoundException;
import kubsu.ru.student.cheker.studentchecker.exeption.FailedLoginUserException;
import kubsu.ru.student.cheker.studentchecker.model.Student;
import kubsu.ru.student.cheker.studentchecker.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentService {
    private final StudentRepository studentRepository;

    public Student getStudentById(Long studentId) {
        Student result = studentRepository.findById(studentId)
                .orElseThrow(EntityNotFoundException.studentNotFound(studentId));
        result.setPassword(null);
        return result;
    }
    public Student getStudentByLogin(String studentLogin) {
        return studentRepository.findStudentByLogin(studentLogin);
    }

    public LoginStudentResponse loginStudent(String login, String password) {
        Student student = studentRepository.findStudentByLogin(login);
        if (!student.getPassword().equals(password)) {
            throw FailedLoginUserException.studentFailedRegister();
        }
        return LoginStudentResponse.fromStudent(student);
    }

    public void addStudent(Student student) {
        student.hashPassword();
        studentRepository.save(student);
    }
}
